FROM openjdk:8-jdk-alpine

VOLUME /tmp

COPY build/libs/demo-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8081

ENTRYPOINT ["java", "-Djava .security.egd=filape:/dev/./urandom", "-jar", "/app.jar"]
